 
# Cloud Photo Storage

> _A demonstration application which has been built in MVP architecture pattern with a more simple and robust way. It includes use of firebase as a backend platform and a modular approach for making it reusable in future_

**Based on a open source reference : [Firebase Auth MVP](https://github.com/DhytoDev/FirebaseAuth-MVP)**

## Firebase Setup

Get **google-service.json** file from firebase console and put in place _CloudPhotoGallery/app/_

Guide : [https://firebase.google.com/docs/android/setup](Firebase)

## Facebook Setup

Create a facebook android project, get the app id and replace 
facebook_app_id and facebook_content_provider_authorities
value with your App ID  in _CloudPhotoGallery/app/src/main/values/strings.xml/_ file.

Guide : [https://developers.facebook.com/docs/android/getting-started/]

***

### Firebase

* Auth : [https://firebase.google.com/docs/auth/android/start/](Auth)

* Database : [https://firebase.google.com/docs/database/android/start/](Database)

* Storage : [https://firebase.google.com/docs/storage/android/start](Storage)

### MVP

![mvp](https://i.imgur.com/jf6F2pT.png)



Create a 

### Sample

![Home View](https://firebasestorage.googleapis.com/v0/b/clouldphotogallery.appspot.com/o/images%2F1546154094599?alt=media&token=c23ec91c-81ad-4869-a3ce-ffd4e9ccc2a2){width=10%}
![Swipable Photo View](https://firebasestorage.googleapis.com/v0/b/clouldphotogallery.appspot.com/o/images%2F1546154094612?alt=media&token=368a0deb-7f72-4341-a2d1-64bf3df4064d) 
![Upload Photo View](https://firebasestorage.googleapis.com/v0/b/clouldphotogallery.appspot.com/o/images%2F1546154094581?alt=media&token=cd62276a-46a0-4376-8ae5-8612c294bbde)  



