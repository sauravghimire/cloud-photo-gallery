package com.thecode.clouldphotogallery.base;

import android.content.Context;

public interface BaseView {
    Context getContext();
}
