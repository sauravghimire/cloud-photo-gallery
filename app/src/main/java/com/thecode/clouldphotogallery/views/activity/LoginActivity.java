package com.thecode.clouldphotogallery.views.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ProgressBar;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.thecode.clouldphotogallery.R;
import com.thecode.clouldphotogallery.base.BaseActivity;
import com.thecode.clouldphotogallery.model.Repository;
import com.thecode.clouldphotogallery.model.RepositoryImpl;
import com.thecode.clouldphotogallery.presenter.LoginPresenter;
import com.thecode.clouldphotogallery.presenter.LoginPresenterImpl;
import com.thecode.clouldphotogallery.utils.Utils;
import com.thecode.clouldphotogallery.views.LoginView;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;


public class LoginActivity extends BaseActivity implements LoginView {

    private static final String EMAIL = "email";

    @BindView(R.id.login_button)
    LoginButton loginButton;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private LoginPresenter loginPresenter;
    CallbackManager callbackManager = CallbackManager.Factory.create();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        Repository repository = RepositoryImpl.getInstance();

        loginPresenter = new LoginPresenterImpl(repository);
        loginPresenter.attachView(this);
        loginPresenter.checkLogin();
        loginButton.setReadPermissions(Arrays.asList(EMAIL));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                loginPresenter.login(loginResult.getAccessToken().getToken());
            }

            @Override
            public void onCancel() {
                loginError(getString(R.string.user_canceled));
            }

            @Override
            public void onError(FacebookException exception) {
                loginError(exception.getMessage());
            }
        });

    }


    @Override
    public void loginSuccess() {
        Utils.setIntent(this, MainActivity.class);
        finish();
    }

    @Override
    public void loginError(String message) {
        Utils.showMessage(this, message);
    }

    @Override
    public void setProgressVisibility(boolean visibility) {
        if (visibility) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);

        }
    }

    @Override
    public void isLogin(boolean isLogin) {
        if (isLogin) {
            Utils.setIntent(this, MainActivity.class);
            finish();
        }
    }

    @Override
    public Context getContext() {
        return this;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginPresenter.detachView();
    }
}
