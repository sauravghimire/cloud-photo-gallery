package com.thecode.clouldphotogallery.views;

import android.graphics.Bitmap;

import com.thecode.clouldphotogallery.base.BaseView;

import java.io.File;

public interface PostFragmentView extends BaseView {
    void bitmapCreatedSuccessfully(Bitmap bitmap);

    void bitmapCreationFailed(String errorMessage);

    void bitmapCreationInProgress();


    void fileDownloadedSuccessfully(File file);

    void fileDownloadFailed(String errorMessage);
}
