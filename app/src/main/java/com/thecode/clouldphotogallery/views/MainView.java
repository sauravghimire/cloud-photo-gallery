package com.thecode.clouldphotogallery.views;

import com.thecode.clouldphotogallery.base.BaseView;

public interface MainView extends BaseView {
    void uploadSuccess();

    void uploadError(String errorMessage);

    void showDialog(String message);

    void signOutSuccessful();

    void dismissDialog();
}
