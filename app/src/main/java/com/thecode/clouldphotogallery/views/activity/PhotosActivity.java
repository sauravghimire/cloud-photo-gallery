package com.thecode.clouldphotogallery.views.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.thecode.clouldphotogallery.R;
import com.thecode.clouldphotogallery.base.BaseActivity;
import com.thecode.clouldphotogallery.model.Post;
import com.thecode.clouldphotogallery.model.Repository;
import com.thecode.clouldphotogallery.model.RepositoryImpl;
import com.thecode.clouldphotogallery.presenter.PhotosPresenter;
import com.thecode.clouldphotogallery.presenter.PhotosPresenterImpl;
import com.thecode.clouldphotogallery.utils.Utils;
import com.thecode.clouldphotogallery.views.PhotosView;
import com.thecode.clouldphotogallery.views.adapters.PhotosPagerAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

public class PhotosActivity extends BaseActivity implements PhotosView {

    private static final String POSITION = "position";
    @BindView(R.id.pager_photos)
    ViewPager pagerPhotos;

    @BindView(R.id.indicator)
    CircleIndicator indicator;
    PhotosPresenter presenter;

    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);
        ButterKnife.bind(this);
        position = getIntent().getIntExtra(POSITION, 0);
        Repository repository = RepositoryImpl.getInstance();
        presenter = new PhotosPresenterImpl(repository);
        presenter.attachView(this);
        presenter.getPosts();

    }

    public static void openPhotos(Context context, int position) {
        Intent intent = new Intent(context, PhotosActivity.class);
        intent.putExtra(POSITION, position);
        context.startActivity(intent);
    }


    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void photoFetchedSuccessfully(List<Post> posts) {
        pagerPhotos.setAdapter(new PhotosPagerAdapter(getSupportFragmentManager(), posts));
        indicator.setViewPager(pagerPhotos);
        pagerPhotos.setCurrentItem(position);
    }

    @Override
    public void photoFetchError(String errorMessage) {
        Utils.showMessage(this, errorMessage);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
