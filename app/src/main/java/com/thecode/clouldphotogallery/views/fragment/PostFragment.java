package com.thecode.clouldphotogallery.views.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.squareup.picasso.Picasso;
import com.thecode.clouldphotogallery.R;
import com.thecode.clouldphotogallery.model.Post;
import com.thecode.clouldphotogallery.model.RepositoryImpl;
import com.thecode.clouldphotogallery.presenter.PostFragmentPresenter;
import com.thecode.clouldphotogallery.presenter.PostFragmentPresenterImpl;
import com.thecode.clouldphotogallery.utils.Utils;
import com.thecode.clouldphotogallery.views.PostFragmentView;

import java.io.File;
import java.io.FileNotFoundException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class PostFragment extends Fragment implements PostFragmentView {
    private static final String POST = "post";
    private static final int REQUEST_CODE_FOR_WRITE_EXTERNAL_STORAGE_PERMISSION = 1001;
    @BindView(R.id.iv_photo)
    AppCompatImageView ivPhoto;
    @BindView(R.id.tv_caption)
    AppCompatTextView tvCaption;
    Unbinder unbinder;
    Post post;

    ProgressDialog dialog;

    CallbackManager callbackManager;
    ShareDialog shareDialog;

    PostFragmentPresenter postFragmentPresenter;

    public static PostFragment newInstance(Post post) {
        PostFragment fragment = new PostFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(POST, post);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        postFragmentPresenter = new PostFragmentPresenterImpl(RepositoryImpl.getInstance());
        postFragmentPresenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        post = getArguments().getParcelable(POST);

        setUpProgressDialog();
        setUpFacebookShare();
        loadPost();
    }

    private void loadPost() {
        Picasso.get()
                .load(post.getDownloadUrl())
                .into(ivPhoto);

        if (!(post.getCaption() == null || post.getCaption().isEmpty())) {
            tvCaption.setVisibility(View.VISIBLE);
            tvCaption.setText(post.getCaption());
        }
    }

    private void setUpFacebookShare() {
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                dialog.dismiss();
                Utils.showMessage(getActivity(), getString(R.string.photo_share_successful));
            }

            @Override
            public void onCancel() {
                Utils.showMessage(getActivity(), getString(R.string.photo_sharing_canceled));
            }

            @Override
            public void onError(FacebookException error) {

                Utils.showMessage(getActivity(), getString(R.string.photo_share_failed));
            }
        });
    }

    private void setUpProgressDialog() {
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getString(R.string.please_wait));
        dialog.setTitle(getString(R.string.share_photo));
        dialog.setCancelable(false);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.photos_menu, menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.share:
                postFragmentPresenter.getBitmap(post.getDownloadUrl());
                break;
            case R.id.download:
                if (Build.VERSION.SDK_INT >= 23) {
                    if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        postFragmentPresenter.download(post.getDownloadUrl());
                    } else {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_FOR_WRITE_EXTERNAL_STORAGE_PERMISSION);
                    }
                } else {
                    postFragmentPresenter.download(post.getDownloadUrl());
                }

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void shareOnFacebook(Bitmap bitmap) {
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(bitmap)
                .build();
        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();
        shareDialog.show(content);
    }

    @Override
    public void bitmapCreatedSuccessfully(Bitmap bitmap) {
        shareOnFacebook(bitmap);
    }

    @Override
    public void bitmapCreationFailed(String errorMessage) {
        dialog.dismiss();
        Utils.showMessage(getActivity(), errorMessage);
    }

    @Override
    public void bitmapCreationInProgress() {
        dialog.show();
    }

    @Override
    public void fileDownloadedSuccessfully(File file) {
        try {
            MediaStore.Images.Media.insertImage(getActivity().
                    getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
            Utils.showMessage(getActivity(), getString(R.string.file_saved));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Utils.showMessage(getActivity(), getString(R.string.download_failed));
        }
    }

    @Override
    public void fileDownloadFailed(String errorMessage) {
        Utils.showMessage(getActivity(), getString(R.string.download_failed));
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        postFragmentPresenter.detachView();
    }

    @Nullable
    @Override
    public Context getContext() {
        return getActivity();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_FOR_WRITE_EXTERNAL_STORAGE_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                postFragmentPresenter.download(post.getDownloadUrl());
            }else{
                Utils.showMessage(getActivity(),getString(R.string.permission_req));
            }
        }
    }
}
