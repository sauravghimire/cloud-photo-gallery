package com.thecode.clouldphotogallery.views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.thecode.clouldphotogallery.R;
import com.thecode.clouldphotogallery.views.activity.PhotosActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhotosViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.iv_photo)
    public AppCompatImageView ivPhoto;
    Context context;

    public PhotosViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        context = itemView.getContext();
        itemView.setOnClickListener(v -> {
            PhotosActivity.openPhotos(context, getAdapterPosition());
        });
    }


}
