package com.thecode.clouldphotogallery.views.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.thecode.clouldphotogallery.model.Post;
import com.thecode.clouldphotogallery.views.fragment.PostFragment;

import java.util.List;

public class PhotosPagerAdapter extends FragmentStatePagerAdapter {

    List<Post> posts;

    public PhotosPagerAdapter(FragmentManager fm, List<Post> posts) {
        super(fm);
        this.posts = posts;
    }

    @Override
    public Fragment getItem(int i) {
        return PostFragment.newInstance(posts.get(i));
    }

    @Override
    public int getCount() {
        return posts.size();
    }
}
