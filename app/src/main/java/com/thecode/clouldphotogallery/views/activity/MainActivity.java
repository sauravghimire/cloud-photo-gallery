package com.thecode.clouldphotogallery.views.activity;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;
import com.thecode.clouldphotogallery.R;
import com.thecode.clouldphotogallery.base.BaseActivity;
import com.thecode.clouldphotogallery.model.Post;
import com.thecode.clouldphotogallery.model.Repository;
import com.thecode.clouldphotogallery.model.RepositoryImpl;
import com.thecode.clouldphotogallery.presenter.MainPresenter;
import com.thecode.clouldphotogallery.presenter.MainPresenterImpl;
import com.thecode.clouldphotogallery.utils.Utils;
import com.thecode.clouldphotogallery.views.MainView;
import com.thecode.clouldphotogallery.views.PhotosViewHolder;
import com.thecode.clouldphotogallery.views.adapters.PhotosAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements MainView {

    private static final int PICK_IMAGE = 5000;

    MainPresenter mainPresenter;
    FirebaseAuth firebaseAuth;
    @BindView(R.id.rv_photos)
    RecyclerView rvPhotos;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    ProgressDialog dialog;
    @BindView(R.id.et_caption)
    AppCompatEditText etCaption;
    @BindView(R.id.rv_photos_upload)
    RecyclerView rvPhotosUpload;
    @BindView(R.id.rl_confirmation_layout)
    RelativeLayout rlConfirmationLayout;

    List<Uri> uris;

    FirebaseRecyclerAdapter photosAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setUpProgressDialog();
        firebaseAuth = FirebaseAuth.getInstance();
        Repository repository = RepositoryImpl.getInstance();
        mainPresenter = new MainPresenterImpl(repository);
        mainPresenter.attachView(this);

        fetchAndLoadPhotos();
    }

    private void fetchAndLoadPhotos() {
        Query query = FirebaseDatabase.getInstance().
                getReference(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        FirebaseRecyclerOptions<Post> options = new FirebaseRecyclerOptions.Builder<Post>()
                .setQuery(query, Post.class)
                .build();

        setUpAdapter(options);
    }

    private void setUpAdapter(FirebaseRecyclerOptions<Post> options) {
        photosAdapter = new FirebaseRecyclerAdapter<Post, PhotosViewHolder>(options) {
            @NonNull
            @Override
            public PhotosViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View photoView = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.item_photo, viewGroup, false);
                return new PhotosViewHolder(photoView);
            }

            @Override
            protected void onBindViewHolder(@NonNull PhotosViewHolder holder, int position, @NonNull Post model) {
                Picasso.get()
                        .load(model.getDownloadUrl())
                        .into(holder.ivPhoto);

            }

        };
        rvPhotos.setAdapter(photosAdapter);
    }

    private void setUpProgressDialog() {
        dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.setCancelable(false);
    }

    @Override
    public void uploadSuccess() {
        Utils.showMessage(this, getString(R.string.upload_successful));
    }

    @Override
    public void uploadError(String errorMessage) {
        Utils.showMessage(this, errorMessage);
    }


    @Override
    public void showDialog(String message) {
        dialog.setTitle(message);
        dialog.show();
    }

    @Override
    public void signOutSuccessful() {
        Utils.setIntent(this, LoginActivity.class);
        finish();
    }

    @Override
    public void dismissDialog() {
        dialog.dismiss();
    }


    @Override
    public Context getContext() {
        return this;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            uris = new ArrayList<>();
            if (data.getData() != null)
                uris.add(data.getData());
            else {
                ClipData clipData = data.getClipData();
                assert clipData != null;
                for (int i = 0; i < clipData.getItemCount(); i++) {
                    ClipData.Item item = clipData.getItemAt(i);
                    uris.add(item.getUri());
                }
            }
            if (!uris.isEmpty()) {
                updateUiForConfirmation(uris);
            }
        }

    }

    private void updateUiForConfirmation(List<Uri> uris) {
        if (uris.size() <= 1) {
            etCaption.setVisibility(View.VISIBLE);
        } else {
            etCaption.setVisibility(View.GONE);
        }
        rlConfirmationLayout.setVisibility(View.VISIBLE);
        rvPhotosUpload.setAdapter(new PhotosAdapter(uris));
        fab.hide();
    }


    @OnClick({R.id.fab, R.id.btn_upload})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.fab:
                Utils.pickImage(this, PICK_IMAGE);
                break;
            case R.id.btn_upload:
                rlConfirmationLayout.setVisibility(View.GONE);
                fab.show();
                mainPresenter.upload(etCaption.getText().toString(), uris);
                break;
        }
    }


    @Override
    public void onBackPressed() {
        if (rlConfirmationLayout.getVisibility() == View.VISIBLE) {
            rlConfirmationLayout.setVisibility(View.GONE);
            fab.show();
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        photosAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        photosAdapter.stopListening();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sign_out:
            mainPresenter.signOut();
            break;
        }
        return super.onOptionsItemSelected(item);
    }
}
