package com.thecode.clouldphotogallery.views;

import com.thecode.clouldphotogallery.base.BaseView;
import com.thecode.clouldphotogallery.model.Post;

import java.util.List;

public interface PhotosView extends BaseView {
    void photoFetchedSuccessfully(List<Post> posts);
    void photoFetchError(String errorMessage);
}
