package com.thecode.clouldphotogallery.views;

import com.thecode.clouldphotogallery.base.BaseView;

public interface LoginView extends BaseView {

    void loginSuccess();

    void loginError(String message);

    void setProgressVisibility(boolean visibility);

    void isLogin(boolean isLogin);
}