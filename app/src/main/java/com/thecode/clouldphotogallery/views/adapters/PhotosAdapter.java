package com.thecode.clouldphotogallery.views.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thecode.clouldphotogallery.R;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.PhotosViewHolder> {
    List<Uri> uris;
    Context context;

    public PhotosAdapter(List<Uri> uris) {
        this.uris = uris;
    }

    @NonNull
    @Override
    public PhotosViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        return new PhotosViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_photo, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PhotosViewHolder photosViewHolder, int i) {
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uris.get(i));
            // Log.d(TAG, String.valueOf(bitmap));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (bitmap != null)
            photosViewHolder.ivPhoto.setImageBitmap(bitmap);
    }

    @Override
    public int getItemCount() {
        return uris.size();
    }

    public class PhotosViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_photo)
        AppCompatImageView ivPhoto;

        public PhotosViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
