package com.thecode.clouldphotogallery.model;

import android.graphics.Bitmap;
import android.net.Uri;

import com.thecode.clouldphotogallery.exception.LoginError;

import java.io.File;
import java.util.List;

public interface Repository {

    interface SignOutCallback {
        void signOutSuccessful();
    }

    interface DownloadPhotoCallback {
        void downloadSuccessful(File file);

        void downloadError(String errorMessage);
    }

    interface GetBitmapCallack {
        void preparingForGettingBitmap();

        void getBitmapSuccess(Bitmap bitmap);

        void getBitmapFailed(String errorMessage);
    }

    interface GetPostsCallback {
        void onGetPhotosSuccess(List<Post> posts);

        void onGetPhotoError(String errorMessage);
    }

    interface LoginCallback {
        void onLoginSuccessful();

        void onLoginError(LoginError e);
    }

    interface UploadCallback {
        void onUploadSuccessful();

        void onUploadFailed(String errorMessage);
    }

    void upload(String caption, List<Uri> uris, UploadCallback uploadCallback);

    void login(String accessToken, LoginCallback loginCallback);

    void getPosts(GetPostsCallback getPostsCallback);

    void getBitmap(String photoUrl, GetBitmapCallack getBitmapCallback);

    void download(String downLoadUrl, DownloadPhotoCallback callback);

    boolean isLoggedIn();

    void signOut(SignOutCallback signOutCallback);


}
