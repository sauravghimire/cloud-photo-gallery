package com.thecode.clouldphotogallery.model;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.thecode.clouldphotogallery.exception.LoginError;
import com.thecode.clouldphotogallery.utils.FirebaseUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class RepositoryImpl implements Repository {

    private static Repository repository;

    public static Repository getInstance() {
        if (repository == null) {
            repository = new RepositoryImpl();
        }

        return repository;
    }

    @Override
    public void upload(String caption, List<Uri> uris, UploadCallback uploadCallback) {
        if (isLoggedIn()) {
            List<Post> posts = getPostListFromUris(caption, uris);
            AtomicInteger uploadedFileCount = new AtomicInteger();
            for (Post post : posts) {
                StorageReference reference = FirebaseStorage.getInstance().
                        getReference().child("images").child(String.valueOf(System.currentTimeMillis()));
                UploadTask uploadTask = reference.putFile(post.getUri());
                uploadTask.continueWithTask(uploadImageTask -> {
                    if (!uploadImageTask.isSuccessful()) {
                        uploadCallback.onUploadFailed(uploadImageTask.getException().getMessage());
                    }
                    return reference.getDownloadUrl();
                }).addOnSuccessListener(uri -> {
                    post.setUri(null);
                    post.setDownloadUrl(uri.toString());
                    DatabaseReference databaseReference = FirebaseUtil.getUserReference();
                    databaseReference.push().setValue(post).addOnCompleteListener(setValuesTask -> {
                        if (setValuesTask.isSuccessful()) {
                            uploadedFileCount.getAndIncrement();
                            if (uploadedFileCount.get() == uris.size()) {
                                uploadCallback.onUploadSuccessful();
                            }
                        } else {
                            uploadCallback.onUploadFailed(setValuesTask.getException().getMessage());
                        }
                    });
                });

            }
        }

    }

    @Override
    public void login(String accessToken, LoginCallback loginCallback) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        auth.signInWithCredential(FacebookAuthProvider.getCredential(accessToken)).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                loginCallback.onLoginSuccessful();
            } else {
                loginCallback.onLoginError(new LoginError(task.getException().getMessage()));
            }

        });
    }

    @Override
    public void getPosts(GetPostsCallback callback) {
        DatabaseReference userRef = FirebaseUtil.getUserReference();
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<Post> posts = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    posts.add(snapshot.getValue(Post.class));
                }
                callback.onGetPhotosSuccess(posts);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callback.onGetPhotoError(databaseError.getMessage());
            }
        });
    }

    @Override
    public void getBitmap(String photoUrl, GetBitmapCallack getBitmapCallback) {

        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                getBitmapCallback.getBitmapSuccess(bitmap);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                getBitmapCallback.getBitmapFailed(e.getMessage());
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                getBitmapCallback.preparingForGettingBitmap();
            }
        };

        Picasso.get().load(photoUrl).into(target);

    }

    @Override
    public void download(String downLoadUrl, DownloadPhotoCallback callback) {
        getBitmap(downLoadUrl, new GetBitmapCallack() {
            @Override
            public void preparingForGettingBitmap() {

            }

            @Override
            public void getBitmapSuccess(Bitmap bitmap) {
                Handler handler = new Handler();
                Thread thread = new Thread(() -> {
                    String path = Environment.getExternalStorageDirectory().toString();
                    OutputStream fOut;
                    File file = new File(path, "cps_" + System.currentTimeMillis() + ".jpg");
                    try {
                        fOut = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
                        fOut.flush();
                        fOut.close();
                        handler.post(() -> callback.downloadSuccessful(file));

                    } catch (IOException e) {
                        e.printStackTrace();
                        handler.post(() -> callback.downloadError(e.getMessage()));


                    }

                });
                thread.start();
            }

            @Override
            public void getBitmapFailed(String errorMessage) {

            }
        });
    }


    @Override
    public boolean isLoggedIn() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        return auth.getCurrentUser() != null;
    }

    @Override
    public void signOut(SignOutCallback signOutCallback) {
        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();
        signOutCallback.signOutSuccessful();
    }

    List<Post> getPostListFromUris(String caption, List<Uri> uris) {
        if (uris.size() > 1) {
            caption = null;
        }
        List<Post> posts = new ArrayList<>();
        for (Uri uri : uris) {
            posts.add(getPost(uri, caption));
        }
        return posts;
    }

    private Post getPost(Uri uri, String caption) {
        Post post = new Post();
        post.setCaption(caption);
        post.setUri(uri);
        return post;
    }
}
