package com.thecode.clouldphotogallery.presenter;

import com.thecode.clouldphotogallery.exception.LoginError;
import com.thecode.clouldphotogallery.model.Repository;
import com.thecode.clouldphotogallery.views.LoginView;

public class LoginPresenterImpl implements LoginPresenter {

    private final Repository repository;
    LoginView loginView;

    public LoginPresenterImpl(Repository repository) {
        this.repository = repository;
    }

    @Override
    public void login(String accessToken) {
        loginView.setProgressVisibility(true);

        repository.login(accessToken, new Repository.LoginCallback() {
            @Override
            public void onLoginSuccessful() {
                loginView.loginSuccess();
            }

            @Override
            public void onLoginError(LoginError e) {
                loginView.setProgressVisibility(false);
                loginView.loginError(e.getMessage());

            }
        });
    }


    @Override
    public void checkLogin() {
        if (repository.isLoggedIn()) {
            loginView.isLogin(true);
        } else {
            loginView.isLogin(false);
        }
    }

    @Override
    public void attachView(LoginView view) {
        loginView = view;
    }

    @Override
    public void detachView() {
        loginView = null;
    }


}
