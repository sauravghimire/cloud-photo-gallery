package com.thecode.clouldphotogallery.presenter;

import com.thecode.clouldphotogallery.base.BasePresenter;
import com.thecode.clouldphotogallery.views.PostFragmentView;

public interface PostFragmentPresenter extends BasePresenter<PostFragmentView> {
    void getBitmap(String url);

    void download(String downloadUrl);
}
