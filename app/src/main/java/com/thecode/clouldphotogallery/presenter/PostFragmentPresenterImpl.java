package com.thecode.clouldphotogallery.presenter;

import android.graphics.Bitmap;

import com.thecode.clouldphotogallery.model.Repository;
import com.thecode.clouldphotogallery.views.PostFragmentView;

import java.io.File;

public class PostFragmentPresenterImpl implements PostFragmentPresenter {

    Repository repository;
    PostFragmentView view;

    public PostFragmentPresenterImpl(Repository repository) {
        this.repository = repository;
    }

    @Override
    public void getBitmap(String url) {
        repository.getBitmap(url, new Repository.GetBitmapCallack() {
            @Override
            public void preparingForGettingBitmap() {
                view.bitmapCreationInProgress();
            }

            @Override
            public void getBitmapSuccess(Bitmap bitmap) {
                view.bitmapCreatedSuccessfully(bitmap);
            }

            @Override
            public void getBitmapFailed(String errorMessage) {
                view.bitmapCreationFailed(errorMessage);
            }
        });
    }

    @Override
    public void download(String downloadUrl) {
        repository.download(downloadUrl, new Repository.DownloadPhotoCallback() {
            @Override
            public void downloadSuccessful(File file) {
                view.fileDownloadedSuccessfully(file);
            }

            @Override
            public void downloadError(String errorMessage) {
                view.fileDownloadFailed(errorMessage);
            }
        });
    }

    @Override
    public void attachView(PostFragmentView view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }
}
