package com.thecode.clouldphotogallery.presenter;

import android.net.Uri;

import com.thecode.clouldphotogallery.model.Repository;
import com.thecode.clouldphotogallery.views.MainView;

import java.util.List;

public class MainPresenterImpl implements MainPresenter {

    private final Repository repository;
    private MainView mainView;

    public MainPresenterImpl(Repository repository) {
        this.repository = repository;
    }


    @Override
    public void upload(String caption, List<Uri> uris) {
        mainView.showDialog("Uploading Image");
        repository.upload(caption, uris, new Repository.UploadCallback() {
            @Override
            public void onUploadSuccessful() {
                mainView.uploadSuccess();
                mainView.dismissDialog();
            }

            @Override
            public void onUploadFailed(String errorMessage) {
                mainView.uploadError(errorMessage);
                mainView.dismissDialog();
            }
        });
    }

    @Override
    public void signOut() {
        repository.signOut(() -> mainView.signOutSuccessful());
    }


    @Override
    public void attachView(MainView view) {
        mainView = view;
    }

    @Override
    public void detachView() {
        mainView = null;
    }
}
