package com.thecode.clouldphotogallery.presenter;

import com.thecode.clouldphotogallery.base.BasePresenter;
import com.thecode.clouldphotogallery.views.PhotosView;

public interface PhotosPresenter extends BasePresenter<PhotosView> {

    void getPosts();

}
