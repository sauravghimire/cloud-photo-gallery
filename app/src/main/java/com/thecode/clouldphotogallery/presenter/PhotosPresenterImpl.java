package com.thecode.clouldphotogallery.presenter;

import com.thecode.clouldphotogallery.model.Post;
import com.thecode.clouldphotogallery.model.Repository;
import com.thecode.clouldphotogallery.views.PhotosView;

import java.util.List;

public class PhotosPresenterImpl implements PhotosPresenter {

    Repository repository;
    PhotosView photosView;

    public PhotosPresenterImpl(Repository repository) {
        this.repository = repository;
    }

    @Override
    public void getPosts() {
        repository.getPosts(new Repository.GetPostsCallback() {
            @Override
            public void onGetPhotosSuccess(List<Post> posts) {
                photosView.photoFetchedSuccessfully(posts);
            }

            @Override
            public void onGetPhotoError(String errorMessage) {
                photosView.photoFetchError(errorMessage);
            }
        });
    }

    @Override
    public void attachView(PhotosView view) {
        photosView = view;
    }

    @Override
    public void detachView() {
        photosView = null;
    }
}
