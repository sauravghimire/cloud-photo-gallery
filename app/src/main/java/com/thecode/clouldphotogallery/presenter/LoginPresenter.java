package com.thecode.clouldphotogallery.presenter;

import com.thecode.clouldphotogallery.base.BasePresenter;
import com.thecode.clouldphotogallery.views.LoginView;

public interface LoginPresenter extends BasePresenter<LoginView> {

    void login(String string);

    void checkLogin();
}
