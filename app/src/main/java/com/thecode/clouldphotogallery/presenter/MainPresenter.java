package com.thecode.clouldphotogallery.presenter;

import android.net.Uri;

import com.thecode.clouldphotogallery.base.BasePresenter;
import com.thecode.clouldphotogallery.views.MainView;

import java.util.List;

public interface MainPresenter extends BasePresenter<MainView> {
    void upload(String caption, List<Uri> uris);
    void signOut();

}
