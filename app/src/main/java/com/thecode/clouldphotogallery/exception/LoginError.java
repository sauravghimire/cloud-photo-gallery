package com.thecode.clouldphotogallery.exception;

public class LoginError extends Exception {
    public LoginError(String detailMessage) {
        super(detailMessage);
    }
}