package com.thecode.clouldphotogallery;

import com.thecode.clouldphotogallery.model.Post;
import com.thecode.clouldphotogallery.model.Repository;
import com.thecode.clouldphotogallery.presenter.PhotosPresenter;
import com.thecode.clouldphotogallery.presenter.PhotosPresenterImpl;
import com.thecode.clouldphotogallery.views.PhotosView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.verify;

public class PhotosPresenterTest {
    @Mock
    private
    Repository repository;

    @Mock
    private PhotosView photosView;

    private PhotosPresenter photosPresenter;

    @Captor
    private ArgumentCaptor<Repository.GetPostsCallback> getPostsCallbackArgumentCaptor;


    @Before
    public void setupLoginPresenter(){
        MockitoAnnotations.initMocks(this);

        photosPresenter = new PhotosPresenterImpl(repository);
        photosPresenter.attachView(photosView);
    }


    @Test
    public void getPosts_GetPostsSuccess(){

        photosPresenter.getPosts();


        verify(repository).getPosts(getPostsCallbackArgumentCaptor.capture());
        List<Post> testPosts = anyList();
        getPostsCallbackArgumentCaptor.getValue().onGetPhotosSuccess(testPosts);

        verify(photosView).photoFetchedSuccessfully(testPosts);
    }

    @Test
    public void getPosts_GetPostsFailed() {

        photosPresenter.getPosts();


        verify(repository).getPosts(getPostsCallbackArgumentCaptor.capture());
        String errorMessage = "Error Message";
        getPostsCallbackArgumentCaptor.getValue().onGetPhotoError(errorMessage);

        verify(photosView).photoFetchError(errorMessage);
    }


}
