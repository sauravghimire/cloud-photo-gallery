package com.thecode.clouldphotogallery;

import android.graphics.Bitmap;

import com.thecode.clouldphotogallery.model.Repository;
import com.thecode.clouldphotogallery.presenter.PostFragmentPresenter;
import com.thecode.clouldphotogallery.presenter.PostFragmentPresenterImpl;
import com.thecode.clouldphotogallery.views.PostFragmentView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.File;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

public class PostFragmentPresenterTest {
    @Mock
    Repository repository;

    @Mock
    private PostFragmentView postFragmentView;

    private PostFragmentPresenter postFragmentPresenter;

    @Captor
    private ArgumentCaptor<Repository.DownloadPhotoCallback> downloadPhotoCallbackArgumentCaptor;

    @Captor
    private ArgumentCaptor<Repository.GetBitmapCallack> getBitmapCallackArgumentCaptor;


    @Before
    public void setupPostFragmentPresenter() {
        MockitoAnnotations.initMocks(this);

        postFragmentPresenter = new PostFragmentPresenterImpl(repository);
        postFragmentPresenter.attachView(postFragmentView);
    }


    @Test
    public void getBitmap_BitmapSuccessfullyReturnedToUI() {

        postFragmentPresenter.getBitmap("imageUrl");


        verify(repository).getBitmap(any(String.class), getBitmapCallackArgumentCaptor.capture());
        Bitmap testBitmap = any(Bitmap.class);
        getBitmapCallackArgumentCaptor.getValue().getBitmapSuccess(testBitmap);
        verify(postFragmentView).bitmapCreatedSuccessfully(testBitmap);
    }

    @Test
    public void getBitmap_BitmapFailedToReturn() {
        postFragmentPresenter.getBitmap("imageUrl");

        verify(repository).getBitmap(any(String.class), getBitmapCallackArgumentCaptor.capture());
        String errorMessage = "Error message";
        getBitmapCallackArgumentCaptor.getValue().getBitmapFailed(errorMessage);
        verify(postFragmentView).bitmapCreationFailed(errorMessage);
    }

    @Test
    public void getBitmap_PreparingToCreateBitmap() {
        postFragmentPresenter.getBitmap("imageUrl");

        verify(repository).getBitmap(any(String.class), getBitmapCallackArgumentCaptor.capture());
        getBitmapCallackArgumentCaptor.getValue().preparingForGettingBitmap();
        verify(postFragmentView).bitmapCreationInProgress();
    }


    @Test
    public void download_DownloadSuccessful() {
        postFragmentPresenter.download("imageUrl");

        verify(repository).download(any(String.class), downloadPhotoCallbackArgumentCaptor.capture());
        File testFile = any(File.class);
        downloadPhotoCallbackArgumentCaptor.getValue().downloadSuccessful(testFile);
        verify(postFragmentView).fileDownloadedSuccessfully(testFile);
    }

    @Test
    public void download_DownloadFailed() {
        postFragmentPresenter.download("imageUrl");

        verify(repository).download(any(String.class), downloadPhotoCallbackArgumentCaptor.capture());
        String errorMessage = "Error message";
        downloadPhotoCallbackArgumentCaptor.getValue().downloadError(errorMessage);
        verify(postFragmentView).fileDownloadFailed(errorMessage);
    }


}
