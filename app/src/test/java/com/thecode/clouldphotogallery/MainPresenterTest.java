package com.thecode.clouldphotogallery;

import com.thecode.clouldphotogallery.model.Repository;
import com.thecode.clouldphotogallery.presenter.MainPresenter;
import com.thecode.clouldphotogallery.presenter.MainPresenterImpl;
import com.thecode.clouldphotogallery.views.MainView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.verify;

public class MainPresenterTest {
    @Mock
    Repository repository;

    @Mock
    private MainView mainView;

    private MainPresenter mainPresenter;

    @Captor
    private ArgumentCaptor<Repository.UploadCallback> uploadCallbackArgumentCaptor;

    @Captor
    private ArgumentCaptor<Repository.SignOutCallback> signOutCallbackArgumentCaptor;


    @Before
    public void setUpMainPresenter() {
        MockitoAnnotations.initMocks(this);

        mainPresenter = new MainPresenterImpl(repository);
        mainPresenter.attachView(mainView);
    }


    @Test
    public void upload_ValidUploadShowSuccessUI() {

        mainPresenter.upload("caption", new ArrayList<>());


        verify(repository).upload(any(String.class), anyList(), uploadCallbackArgumentCaptor.capture());

        uploadCallbackArgumentCaptor.getValue().onUploadSuccessful();

        verify(mainView).uploadSuccess();
    }

    @Test
    public void login_InvalidLoginShowErrorUI() {

        mainPresenter.upload("caption", new ArrayList<>());


        verify(repository).upload(any(String.class), anyList(), uploadCallbackArgumentCaptor.capture());


        String errorMessage = "Upload Error";
        uploadCallbackArgumentCaptor.getValue().onUploadFailed(errorMessage);

        verify(mainView).uploadError(errorMessage);
    }

    @Test
    public void signOut_SignOutSuccess() {

        mainPresenter.upload("caption", new ArrayList<>());


        verify(repository).signOut(signOutCallbackArgumentCaptor.capture());

        signOutCallbackArgumentCaptor.getValue().signOutSuccessful();

        verify(mainView).signOutSuccessful();
    }


}
