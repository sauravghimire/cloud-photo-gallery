package com.thecode.clouldphotogallery;

import com.thecode.clouldphotogallery.exception.LoginError;
import com.thecode.clouldphotogallery.model.Repository;
import com.thecode.clouldphotogallery.presenter.LoginPresenter;
import com.thecode.clouldphotogallery.presenter.LoginPresenterImpl;
import com.thecode.clouldphotogallery.views.LoginView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

public class LoginPresenterTest {
    @Mock
    private
    Repository repository;

    @Mock
    private LoginView loginView;

    private LoginPresenter loginPresenter;

    @Captor
    private ArgumentCaptor<Repository.LoginCallback> loginCallbackArgumentCaptor;


    @Before
    public void setupLoginPresenter(){
        MockitoAnnotations.initMocks(this);

        loginPresenter = new LoginPresenterImpl(repository);
        loginPresenter.attachView(loginView);
    }


    @Test
    public void login_ValidLoginShowSuccessUI(){

        loginPresenter.login("accessToken");


        verify(repository).login(any(String.class), loginCallbackArgumentCaptor.capture());

        loginCallbackArgumentCaptor.getValue().onLoginSuccessful();

        verify(loginView).loginSuccess();
    }

    @Test
    public void login_InvalidLoginShowErrorUI(){

        loginPresenter.login("accessToken");


        verify(repository).login(any(String.class), loginCallbackArgumentCaptor.capture());


        String errorMessage = "Login Error";
        loginCallbackArgumentCaptor.getValue().onLoginError(new LoginError(errorMessage));

        verify(loginView).loginError(errorMessage);
    }

}
